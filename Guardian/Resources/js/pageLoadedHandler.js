
function elementModifier() {
    let clicked = false;
    if (window.webkit !== null) {
        let linkElements = document.links;
        for (var i = 0; i < linkElements.length; i++) {
            let currentLink = linkElements[i];
            let intervalID = 0;
            currentLink.ontouchstart = (e) => {
                e.preventDefault();
                clicked = false;
                intervalID = window.setInterval(() => {
                    clicked = true;
                    window.clearInterval(intervalID);
                    window.webkit.messageHandlers.linkLongPressHandler.postMessage(JSON.stringify({x: currentLink.offsetLeft, y: currentLink.offsetTop + currentLink.offsetHeight, href: e.target.href}));
                }, 1000)
            };
            currentLink.ontouchend = (e) => {
                window.clearInterval(intervalID);
                if (clicked === false) {
                    let click = new MouseEvent("click", {
                        bubbles: true,
                        cancelable: true,
                        view: window
                      });
                    e.target.dispatchEvent(click)
                }
            };
            currentLink.ontouchcancel = (e) => {
                window.clearInterval(intervalID);
                if (clicked === false) {
                    let click = new MouseEvent("click", {
                        bubbles: true,
                        cancelable: true,
                        view: window
                    });
                    e.target.dispatchEvent(click)
                }
            }
        }
    } else {
        return;
    }
}

window.addEventListener('load', elementModifier);