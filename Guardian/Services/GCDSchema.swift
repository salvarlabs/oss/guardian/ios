//
//  GCDSchema.swift
//  Guardian
//
//  Created by Saulo on 30/04/21.
//

import CoreData

/**
 Relevant entities and attributes in the Core Data schema.
 */
enum Schema {
    enum GCDTab: String {
        case id, title, encodedImage
    }
}
