//
//  MockEnvService.swift
//  Guardian
//
//  Created by Saulo on 11/04/21.
//

import Foundation

enum ApplicationEnvironment: Int {
    case Debug = 0
    case Release = 1
}

class MockEnvService: NSObject {
    
    let environment: ApplicationEnvironment = ProcessInfo.processInfo.environment["ISDEV"] == "1" ? .Debug : .Release;
    
    func valueForEnvironment<T>(debugValue: T, releaseValue: T) -> T {
        return self.environment == ApplicationEnvironment.Debug ? debugValue : releaseValue
    }
}
