//
//  GCDTabProvider.swift
//  Guardian
//
//  Created by Saulo on 30/04/21.
//


import UIKit
import CoreData

class TabProvider {
    private(set) var persistentContainer: NSPersistentContainer
    private weak var fetchedResultsControllerDelegate: NSFetchedResultsControllerDelegate?
    
    init(with persistentContainer: NSPersistentContainer,
         fetchedResultsControllerDelegate: NSFetchedResultsControllerDelegate?) {
        self.persistentContainer = persistentContainer
        self.fetchedResultsControllerDelegate = fetchedResultsControllerDelegate
    }
    
    convenience init(with persistentContainer: NSPersistentContainer) {
        self.init(with: persistentContainer, fetchedResultsControllerDelegate: nil)
    }
    
    /**
     A fetched results controller for the Tab entity, sorted by name.
     */
    lazy var fetchedResultsController: NSFetchedResultsController<GCDTab> = {
        let fetchRequest: NSFetchRequest<GCDTab> = GCDTab.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Schema.GCDTab.title.rawValue, ascending: false)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = fetchedResultsControllerDelegate
        
        do {
            try controller.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("###\(#function): Failed to performFetch: \(nserror), \(nserror.userInfo)")
        }
        
        return controller
    }()
    
    /**
     The number of tags. Used for tag name input validation.
     */
    func numberOfTabs() -> Int {
        let fetchRequest: NSFetchRequest<GCDTab> = GCDTab.fetchRequest()        
        let number = try? persistentContainer.viewContext.count(for: fetchRequest)
        return number ?? 0
    }
    
    /**
     Get all tabs
     */
    
    func getTabs() -> [GCDTab]? {
        let fetchRequest: NSFetchRequest<GCDTab> = GCDTab.fetchRequest()
        let tabs = try? persistentContainer.viewContext.fetch(fetchRequest)
        return tabs ?? nil
        
    }
    
    /**
     Get tab by ID.
     */
    
    func getTabByID(id: UUID, with context: NSManagedObjectContext?) -> GCDTab? {
        let fetchRequest: NSFetchRequest = GCDTab.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", id.uuidString)
        fetchRequest.predicate = predicate
        let context = context ?? persistentContainer.viewContext
        do {
            let tab = try context.fetch(fetchRequest)
            print(tab)
            return tab.first as? GCDTab ?? nil
        } catch {
            print(error)
            return nil
        }
    }
    
    /**
     Add a tab.
     */
    func addTab(title: String, url: String, context: NSManagedObjectContext, shouldSave: Bool = true)-> GCDTab? {
        var id: UUID?
        
        context.performAndWait {
            let tab = GCDTab(context: context)
            tab.title = title
            tab.url = url
            id = tab.id
            if shouldSave {
                context.save(with: .addTab)
                try? persistentContainer.viewContext.save()
            }
        }
        return self.getTabByID(id: id!, with: context) ?? nil
    }
    
    func updateTab(with tab: GCDTab, context: NSManagedObjectContext) {
        persistentContainer.performBackgroundTask { (contextObj) in
            guard let tabID = tab.id, let tabInData = self.getTabByID(id:tabID, with: context) else {return}
            tabInData.title = tab.title
            tabInData.url = tab.url
            tabInData.encodedImage = tab.encodedImage
            try? context.save()
        }
        
    }
    
    /**
     Delete tab.
     */
    func deleteTab(at indexPath: IndexPath, shouldSave: Bool = true) {
        let context = fetchedResultsController.managedObjectContext
        context.performAndWait {
            context.delete(fetchedResultsController.object(at: indexPath))
            if shouldSave {
                context.save(with: .deleteTab)
            }
        }
    }
    
    func deleteTabByID(_ tab: GCDTab, context: NSManagedObjectContext) {
        guard let tabID = tab.id, let tabInData = self.getTabByID(id:tabID, with: context) else {return}
        context.performAndWait {
            context.delete(tabInData)
            context.save(with: .deleteTab)
        }
    }
    
    /**
     Delete all tabs and their info in the background using a batch delete request.
     */
    func batchDeleteAllTabs(completionHandler: (() -> Void)? = nil) {
        let taskContext = persistentContainer.backgroundContext()
        taskContext.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: GCDTab.entity().name!)
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            batchDeleteRequest.resultType = .resultTypeCount
            do {
                let batchDeleteResult = try taskContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
                print("###\(#function): Batch deleted tabs count: \(String(describing: batchDeleteResult?.result))")
            } catch {
                print("###\(#function): Failed to batch delete existing records: \(error)")
            }
            completionHandler?()
        }
    }
}
