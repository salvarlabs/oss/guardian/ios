//
//  GCDTab.swift
//  Guardian
//
//  Created by Saulo on 25/04/21.
//

import Foundation
import CoreData

@objc(GCDTab)
class GCDTab: NSManagedObject {
    override func awakeFromInsert() {
        super.awakeFromInsert()
        self.id = UUID()
    }
}

@objc
protocol GCDManagedObject: class {
    var id: UUID? {get set}
    static func fetchRequest() -> NSFetchRequest<NSFetchRequestResult>
    static var attributes: [String] {get}
}

extension GCDTab: GCDManagedObject {
    static var attributes = [
        "imagePath",
        "id",
        "encodedImage",
        "title",
        "url",
    ]
}
