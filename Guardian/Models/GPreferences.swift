//
//  GPreferences.swift
//  Guardian
//
//  Created by Saulo on 25/04/21.
//

import Foundation

enum GPreferences {
    enum GPreferencesUITheme {
        case light
        case dark
        case systemDefined
    }
    case adblock
    case standardNSFWFilter
}

class GPreferencesCoordinator {
    let currentPrefs = UserDefaults.standard
    let options: [GPreferences: String] = [:]
}
