//
//  TabCollectionViewCellData.swift
//  Guardian
//
//  Created by Saulo on 02/05/21.
//

import Foundation
import UIKit

struct TabCollectionViewCellData: Equatable {
    var title: String
    var thumbnail: UIImage?
    init(title: String, thumbnail: UIImage?) {
        self.title = title
        self.thumbnail = thumbnail
    }
}
