//
//  GWKElement.swift
//  Guardian
//
//  Created by Saulo on 18/04/21.
//

import Foundation

protocol GWKElement: Codable {
    var x: Double {get set}
    var y: Double {get set}
}

struct GWKLink: GWKElement {
    var x: Double
    var y: Double
    var href: String?
}
