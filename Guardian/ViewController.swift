//
//  ViewController.swift
//  Guardian
//
//  Created by saulo.santos.freire on 16/01/21.
//

import UIKit
import WebKit
import GuardianBlockLists
import CoreData

class ViewController: UIViewController {

    var webView: WKWebView!
    var dataContainer: NSPersistentContainer!
    var context: NSManagedObjectContext!
    
    let viewForCustomButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    let labelForCustomView = UILabel()
    var tabID: UUID?
    var currentTabData: GCDTab?
    var openTabs: Int = 0 {
        didSet {
            if oldValue != openTabs {
                labelForCustomView.text = "\(openTabs)"
            }
        }
    }
    
    lazy var tabProvider: TabProvider = {
        let provider = (UIApplication.shared.delegate as! AppDelegate).tabProvider
        return provider
    }()
    
    @IBOutlet weak var addressBar: UITextField!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var headerStackView: UIStackView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var containerStackView: UIStackView!
    
    lazy var navBarTitleView: GWKNavTitleView = {
        let titleView = GWKNavTitleView()
        return titleView
    }()
    
    lazy var headerHeight: NSLayoutConstraint = { [unowned self] in
        let height = NSLayoutConstraint(item: headerView!, attribute: .height, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: 10)
        return height
    }()
    var initialURL: String?
    var linkMenuName: String?
    var initialContentOffset: CGFloat = 0.0
    var scrollDirection: ScrollDirection = .up {
        didSet {
            print(scrollDirection)
            if oldValue != scrollDirection || (scrollDirection == .up && headerHeight.constant == 10 || scrollDirection == .down && headerHeight.constant == 80) {
                if scrollDirection == .down {
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut]) { [weak self] in
                        guard let headerView = self?.headerView else {return}
                        let newFrame = CGRect(x: headerView.frame.minX, y: headerView.frame.minY, width: headerView.frame.width, height: 10)
                        headerView.frame = newFrame
                    } completion: { (_) in
                        self.headerHeight.constant = 10
                        self.navBarTitleView.showTitle = true
                        self.navigationController?.isToolbarHidden = true
                    }
                } else {
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut]) { [weak self] in
                        guard let headerView = self?.headerView else {return}
                        let newFrame = CGRect(x: headerView.frame.minX, y: headerView.frame.minY, width: headerView.frame.width, height: 80)
                        headerView.frame = newFrame
                    } completion: { (_) in
                        self.headerHeight.constant = 80
                        self.navBarTitleView.showTitle = false
                        self.navigationController?.isToolbarHidden = false
                    }
                }
            }
        }
    }
    
    enum ScrollDirection {
        case up
        case left
        case right
        case down
    }
    
    enum GuardianUIState {
        case normal
        case danger
    }
    
    let httpUriRegex = #"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"#
    let httplessUriRegex = #"[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"#
    let customHTML = """
        <html>
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Blocked</title>
            </head>
            <style>
                body {
                  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
                }
            </style>
            <body>
                <h1>Blocked</h1>
            </body>
        </html>
    """
    var WKWebViewConstraints = [NSLayoutConstraint]()
    enum BlockType {
        case host
        case url
        case keyword
    }
    let openSettingsHandler = "openSettingsHandler"
    let themeColorHandler = "themeColorHandler"
    let linkLongPressHandler = "linkLongPressHandler"
    var coloredUI = false
    var lastURL = ""
//    let gRed = UIColor(red: 200, green: 22, blue: 22, alpha: 1.0)
    let gRed = UIColor(red: 0.78, green: 0.09, blue: 0.09, alpha: 1.00)
    override func loadView() {
        super.loadView()
        
        let _ = """
            [
                {
                    "trigger": {
                        "url-filter": ".*",
                        "if-domain": ["*.medium.com"]
                    },
                    "action": {
                        "type": "css-display-none",
                        "selector": ".overlay"
                    }
                }
            ]
        """
        let _ = """
            function getThemeColor() {
                var color = ""
                var metas = Array.prototype.slice.call(document.getElementsByTagName("meta"));
                if (metas.length > 0) {
                    metas.forEach(tag => {
                        if (tag.hasAttribute("name") && tag.getAttribute("name") === "theme-color") {
                            alert("There was a color!")
                            color = tag.getAttribute("content");
                        }
                    });
                    if (color.length > 0) {
                        if (window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.themeColorHandler) {
                            window.webkit.messageHandlers.themeColorHandler(color);
                        } else {
                            alert("Guardian WebView not found.")
                        }
                    } else {
                        alert("Color not found.")
                    }
                } else {
                    alert("Couldn't get meta tags")
                }
            }
            window.addEventListener("DOMContentLoaded", getThemeColor)
        """
//        MARK: -PRE iOS 13 Action Sheet Handler
//        let jsPath = Bundle.main.path(forResource: "js/pageLoadedHandler", ofType: "js")
//        let jsContent = try! String.init(contentsOfFile: jsPath!)
//        let userScript = WKUserScript.init(source: jsContent, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
//        MARK: -SafariExtension Trial
//        WKContentRuleListStore.default()
//            .compileContentRuleList(forIdentifier: "ContentBlockingRules", encodedContentRuleList: json) {
//            (contentRuleList, error) in
//                guard let contentRuleList = contentRuleList, error == nil else { print(error?.localizedDescription); return }
                let configuration = WKWebViewConfiguration()
                configuration.userContentController.add(self, name: self.openSettingsHandler)
        configuration.userContentController.add(self, name: self.themeColorHandler)
        configuration.userContentController.add(self, name:self.linkLongPressHandler)
//        configuration.userContentController.addUserScript(userScript)
        configuration.allowsAirPlayForMediaPlayback = true
        configuration.allowsInlineMediaPlayback = true
        configuration.allowsPictureInPictureMediaPlayback = true
        configuration.applicationNameForUserAgent = "Guardian"
        configuration.dataDetectorTypes = .all
        configuration.mediaTypesRequiringUserActionForPlayback = .all
        configuration.selectionGranularity = .character
        let prefs = WKPreferences()
        if #available(iOS 13.0, *) {
            prefs.isFraudulentWebsiteWarningEnabled = true
        } else {
            // Fallback on earlier versions
        }
        prefs.javaScriptEnabled = true
        configuration.preferences = prefs
        if #available(iOS 13.0, *) {
            let wpPrefs = WKWebpagePreferences()
            if #available(iOS 14.0, *) {
                wpPrefs.allowsContentJavaScript = true
            } else {
                // Fallback on earlier versions
            }
            wpPrefs.preferredContentMode = .recommended
            configuration.defaultWebpagePreferences = wpPrefs
        } else {
            // Fallback on earlier versions
        }
        
//                configuration.userContentController.add(contentRuleList)
//                if #available(iOS 14.0, *) {
//                    configuration.defaultWebpagePreferences.allowsContentJavaScript = true
//                } else {
//                    // Fallback on earlier versions
//                    configuration.preferences.javaScriptEnabled = true
//                }
//        let script = WKUserScript(source: js, injectionTime: .atDocumentStart, forMainFrameOnly: false)
//        configuration.userContentController.addUserScript(script)
                self.webView = WKWebView(frame: self.webViewContainer.frame, configuration: configuration)
        self.webView.isMultipleTouchEnabled = true
        self.webView.scrollView.delegate = self
//            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let appDelegate = (UIApplication.shared.delegate as? AppDelegate) else  {
                fatalError("Data Container not found.")
        }
        dataContainer = GuardianCoreData.shared.persistentContainer
        context = dataContainer.viewContext
        navBarTitleView.addressBarDelegate = self
        navigationItem.titleView = navBarTitleView
        navigationController?.navigationItem.titleView?.alpha = 1.0
        registerNewTab()
        addressBar.delegate = self
        addressBar.autocorrectionType = .no
        addressBar.autocapitalizationType = .none
        addressBar.backgroundColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.20)
        addressBar.layer.cornerRadius = addressBar.frame.size.width / 22
        addressBar.clipsToBounds = true
        webView.load(URLRequest(url: URL(string: initialURL ?? "https://google.com")!))
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        webView.allowsBackForwardNavigationGestures = true
//        if #available(iOS 13.0, *) {
//            let interaction = UIContextMenuInteraction(delegate: self)
//            self.webView.addInteraction(interaction)
//        } else {
//            // Fallback on earlier versions
//            print("no context menu available")
//        }
        setWebViewConstraints()
        var backButton = UIBarButtonItem(barButtonSystemItem: .undo, target: webView, action: #selector(webView.goBack))
        var forwardButton = UIBarButtonItem(barButtonSystemItem: .fastForward, target: webView, action: #selector(webView.goForward))
        if #available(iOS 13.0, *) {
            backButton = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: webView, action: #selector(webView.goBack))
            forwardButton = UIBarButtonItem(image: UIImage(systemName: "chevron.right"), style: .plain, target: webView, action: #selector(webView.goForward))
        }
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        labelForCustomView.text = "\(openTabs.count)"
        labelForCustomView.layer.borderWidth = 2
        labelForCustomView.layer.borderColor = UIColor.systemBlue.cgColor
        labelForCustomView.layer.cornerRadius = 50 / 8
        labelForCustomView.layer.masksToBounds = true
        viewForCustomButton.addSubview(labelForCustomView)
        labelForCustomView.textAlignment = .center
        labelForCustomView.textColor = .systemBlue
        labelForCustomView.translatesAutoresizingMaskIntoConstraints = false
        labelForCustomView.centerYAnchor.constraint(equalTo: viewForCustomButton.centerYAnchor).isActive = true
        labelForCustomView.centerXAnchor.constraint(equalTo: viewForCustomButton.centerXAnchor).isActive = true
        labelForCustomView.widthAnchor.constraint(equalToConstant: CGFloat(35)).isActive = true
        labelForCustomView.heightAnchor.constraint(equalToConstant: CGFloat(35)).isActive = true
        viewForCustomButton.addTarget(self, action: #selector(callTabSelectionCenter), for: .touchUpInside)
        let customViewButton = UIBarButtonItem(customView: viewForCustomButton)
        toolbarItems = [
            spacer,
            backButton,
            spacer,
            forwardButton,
            spacer,
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(callNewDefaultTab)),
            spacer,
            customViewButton,
            spacer,
            refreshButton,
            spacer,
        ]
        navigationController?.isToolbarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        TODO: - Delete tab when ViewController is detached from the navigation stack
//        self.deleteTab()
        super.viewDidDisappear(animated)
    }
    
    deinit {
        self.webView = nil
    }
    
    func fetchTabs() {
        openTabs = tabProvider.numberOfTabs()
    }
    
    func updateTab() {
        var base64Str: String?
        let snapshotConfig = WKSnapshotConfiguration()
        var dataToUpdate = [
            "title": self.webView.title,
            "url": self.webView.url?.absoluteString
        ]
        guard let currentTab = currentTabData, let id = currentTab.id else {return}
//        let fetchRequest: NSFetchRequest<GCDTab> = GCDTab.fetchRequest()
//        let predicate = NSPredicate(format: "id == %@", id.uuidString)
//        fetchRequest.predicate = predicate
//
//        guard let tab = try? tabProvider.persistentContainer.viewContext.fetch(fetchRequest).first else {print("Couldn't fetch for some reason"); return}

        let context = self.tabProvider.persistentContainer.backgroundContext()
                
            self.webView.takeSnapshot(with: snapshotConfig) { (uiImage, error) in
                if (error != nil) {
                    print(error!)
                } else {
                    if let imageData = uiImage?.pngData() {
                        base64Str = imageData.base64EncodedString(options: .lineLength64Characters)
                        print("Updated coredata with image")
                        dataToUpdate["encodedImage"] = base64Str
                        currentTab.title = dataToUpdate["title"]!
                        currentTab.url = dataToUpdate["url"]!
                        currentTab.encodedImage = base64Str
                        self.tabProvider.updateTab(with: currentTab, context: context)
                    }
                    
                }
            }
    }
    
    func deleteTab() {
        guard let tab = currentTabData else {return}
        let context = self.tabProvider.persistentContainer.backgroundContext()
        tabProvider.deleteTabByID(tab, context: context)
    }
    
    func registerNewTab() {
        if currentTabData == nil {
            self.currentTabData = tabProvider.addTab(title: self.title ?? "Google", url: self.initialURL ?? "https://google.com", context: GuardianCoreData.shared.managedObjectContext, shouldSave: true)
        }
        self.fetchTabs()
    }
    
    func debugWKWebViewGestureHandlers() {
        print("Getting recognisers...")
        for subview in self.webView.scrollView.subviews {
            print("Examining subview \(subview)")
            guard let recognisers = subview.gestureRecognizers else {return}
            print("Subview recognisers found")
            for recogniser in recognisers {
                if recogniser.isKind(of: UILongPressGestureRecognizer.self) {
                    
//                    let longPress = recogniser as! UILongPressGestureRecognizer
//                    if longPress.minimumPressDuration > 0.1 {
                        subview.removeGestureRecognizer(recogniser)
//                    }
                }
            }
            
            print("========================================")
            print("--------------- RECOGNIZERS ------------")
            print("========================================")
            print(recognisers)
            
        }
    }
    
    func processRequest(from text: String?) {
        guard let urlString = text,
              let url: URL = URL(string: urlString)
        else {return}

        let urlReq = URLRequest(url: url)
        self.webView.load(urlReq)
    }
    func upgradeURL(with uri: String)->String {
        if uri.range(of:  #"^https:\/\/.*"#, options: .regularExpression) != nil {
            return uri
        } else if uri.range(of: #"^http:\/\/.*"#, options: .regularExpression) != nil {
            // TODO: - Check if TLS is available, then upgrade to https
//            let baseUri = uri.components(separatedBy: "http://")
//            let upgradedUri = "https://\(baseUri[1])"
            return uri
        } else {
            let upgradedUri = "http://\(uri)"
            return upgradedUri
        }
    }
    
    func setWebViewConstraints() {
        self.webViewContainer.addSubview(self.webView)
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.WKWebViewConstraints.append(self.webView.topAnchor.constraint(equalTo: self.webViewContainer.topAnchor))
        self.WKWebViewConstraints.append(self.webView.leadingAnchor.constraint(equalTo: self.webViewContainer.leadingAnchor))
        self.WKWebViewConstraints.append(self.webView.trailingAnchor.constraint(equalTo: self.webViewContainer.trailingAnchor))
        self.WKWebViewConstraints.append(self.webView.bottomAnchor.constraint(equalTo: self.webViewContainer.bottomAnchor))
        NSLayoutConstraint.activate(self.WKWebViewConstraints)
        NSLayoutConstraint.activate([headerHeight])
    }
    
    @IBAction func callTabSelectionCenter(_ sender: UIButton) {
        let tabSelectionCenter = TabSelectionViewController()
        self.show(tabSelectionCenter, sender: sender)
    }
    
    @objc func callNewDefaultTab() {
        let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SBWebViewController")as! ViewController
        newVC.initialURL = "https://google.com"
        try? self.tabProvider.persistentContainer.viewContext.save()
        self.show(newVC, sender: nil)
    }
    func checkIfBlocked(by blockType: BlockType, withURL url: URL? )->Bool {
        guard let newURL = url else {return false}
        switch blockType {
        /// For Ads and stuff.
        case .host:
            guard let hostsPath = GLists.NSCHostList?.path, let host = newURL.host
            else { return false}
            let hostsFile = try? String(contentsOfFile: hostsPath, encoding: String.Encoding.utf8)
            guard let blockedHosts = hostsFile else {print("HOSTS FILE EMPTY"); return false}
            if blockedHosts.contains(host) {
                self.webView.stopLoading()
                return true
            } else {
                print(host)
                guard let adListPath = GLists.adlist?.path, let adList = try? String(contentsOfFile: adListPath, encoding: String.Encoding.utf8) else {return false}
                let domain = host.replacingOccurrences(of: ".", with: #"\."#)
                let pattern = "\n\(domain)\\b"
                let regex = try! NSRegularExpression(pattern: pattern, options: [])
                
                if let _ = regex.firstMatch(in: adList, range: NSRange(location: 0, length: adList.utf16.count)) {
                    return true
                }
                return false
            }
        /// For exclusive pages
        case .url:
            guard let hostsPath = GLists.NSCUrlList?.path else {return false}
            let hostsFile = try? String(contentsOfFile: hostsPath, encoding: String.Encoding.utf8)
            guard let blockedHosts = hostsFile else {return false}
            if blockedHosts.contains(newURL.absoluteString) {
                return true
            }
            return false
        /// For any keyword (dangerous)
        case .keyword:
            guard let hostsPath = GLists.NSCKeywordList?.path
            else { return false }
            let hostsFile = try? String(contentsOfFile: hostsPath, encoding: String.Encoding.utf8)
            guard let words = hostsFile?.components(separatedBy: .newlines) else {return false}
            if words.contains(where: { newURL.absoluteString.contains($0) }) {
                print(newURL.absoluteString)
                return true
            }
            return false
        }
    }
}

extension ViewController: WKNavigationDelegate, WKUIDelegate {
    
    //MARK: -WKNavigationDelegate
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
//        if navigationAction.navigationType == .linkActivated  {
//            if let url = navigationAction.request.url,
//                UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.open(url)
//                print(url)
//                decisionHandler(.cancel)
//            }
//        }
        // MARK: -BlockedByDomain
        if self.checkIfBlocked(by: .host, withURL: navigationAction.request.url) {
            decisionHandler(.cancel)
            return
        }
        // MARK: -BlockedByURL
        if self.checkIfBlocked(by: .url, withURL: navigationAction.request.url) {
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let res = navigationResponse.response as? HTTPURLResponse {
            print(res.statusCode)
        }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        guard let currentURL = webView.url?.absoluteString else {return }
        if currentURL.contains("file://") {
            changeHeaderColor(forState: .danger)
        } else {
            self.addressBar.text = currentURL
            self.navBarTitleView.addressBar.text = currentURL
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        guard let currentURL = webView.url?.absoluteString else {return }
        if !currentURL.contains("file://") {
            changeHeaderColor(forState: .normal)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        self.webView.evaluateJavaScript("document.body.style.webkitTouchCallout='none';") { (result, error) in
//            if error != nil {
//                print("error: \(error?.localizedDescription)")
//            }
//            print("result: \(result)")
//        }
//        debugWKWebViewGestureHandlers()
        
        if let title = self.webView.title {
            self.title = title
            self.navBarTitleView.titleString = title
        }
        
        self.updateTab()
        print("Finished nav")
    }
    
    func changeHeaderColor(forState state: GuardianUIState) {
        switch state {
        case .danger:
            UIView.animate(withDuration: 0.5, delay: 0.01, options: [.curveEaseInOut]) {
                self.view.backgroundColor = self.gRed
                self.headerView.backgroundColor = self.gRed
                self.addressBar.textColor = .white
                self.navigationController?.navigationBar.isTranslucent = false
                self.navigationController?.navigationBar.barTintColor = self.gRed
            }
        default:
            UIView.animate(withDuration: 0.5, delay: 0.15, options: [.curveEaseInOut]) {
                self.view.backgroundColor = .white
                self.headerView.backgroundColor = .clear
                self.addressBar.textColor = .black
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.navigationBar.barTintColor = .white
            }
            break
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let ac = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(ac, animated: true)
        completionHandler()
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        let newVC = ViewController()
        newVC.initialURL = navigationAction.request.url?.absoluteString
        present(newVC, animated: true) {
            print("presented new tab")
        }
        return nil
    }
    
    @available(iOS 13.0, *)
    func webView(_ webView: WKWebView, contextMenuConfigurationForElement elementInfo: WKContextMenuElementInfo, completionHandler: @escaping (UIContextMenuConfiguration?) -> Void) {
        print(elementInfo)
        
        if let link = elementInfo.linkURL {
            linkMenuName = link.absoluteString
            
            let interaction = UIContextMenuInteraction(delegate: self)
            completionHandler(contextMenuInteraction(interaction, configurationForMenuAtLocation: elementInfo.accessibilityActivationPoint))
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
            // MARK: -BlockedByKeyword
            if self.checkIfBlocked(by: .keyword, withURL: self.webView.url) {
                self.webView.stopLoading()
                self.progressView.progress = 0
                self.progressView.alpha = 0
                if let htmlPath = Bundle.main.url(forResource: "std-blocked", withExtension: "html") {
//                    let req = URLRequest(url: htmlPath)
                    self.webView.loadFileURL(htmlPath, allowingReadAccessTo: htmlPath)
//                    self.webView.load(req)
                    self.lastURL = self.webView.url!.absoluteString
                }
                return
            }
        }
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            if (self.webView.estimatedProgress == 1) {
                UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut]) { [weak self] in
                    self?.progressView.alpha = 0
                }
            }
            else {
                UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut]) { [weak self] in
                    self?.progressView.alpha = 1
                }
                progressView.progress = Float(webView.estimatedProgress)

            }
        }
    }
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

extension ViewController: WKScriptMessageHandler {
    //MARK: -WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("CALLED")
        print("MESSAGE FROM WEBVIEW: \(message.body)")
        if message.name == openSettingsHandler {
            print("OPEN SETTINGS CALLED FROM WEBVIEW")
        }
        if message.name == linkLongPressHandler {
            print("LINK: \(message.body)")
            guard let link: String = message.body as? String,
                  let jsonData = link.data(using: .utf8) else {return}
            let linkElement = try? JSONDecoder().decode(GWKLink.self, from: jsonData)
            if let linkSrc = linkElement?.href {
                let sheet = UIAlertController(title: linkSrc, message: nil, preferredStyle: .actionSheet)
                sheet.addAction(UIAlertAction(title: "Open in a new tab", style: .default, handler: { (action) in
                    print("Open in a new tab called by \(action)")
                    let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SBWebViewController")as! ViewController
                    newVC.initialURL = linkSrc
                    self.show(newVC, sender: nil)
//                    self.show(newVC, animated: true) {
//                        print("presented new tab")
//                    }
                }))
                sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    print("Cancel called by \(action)")
                }))
                self.present(sheet, animated: true, completion: nil)
            }
            print(linkElement)
        }
//        if message.name == themeColorHandler {
//            if let hexColor = message.body as? String, let color = UIColor(hex: hexColor) {
//                UIView.animate(withDuration: 0.5, delay: 0.25, options: [.curveEaseInOut]) {
//                    self.headerView.backgroundColor = color
//                }
//            }
//        }
    }
}

extension ViewController: UITextFieldDelegate {
    //MARK: -UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        guard let webAddress = textField.text, let webViewURL = webView.url else {return}
        if reason == .committed {
            if webAddress.range(of: httpUriRegex, options: .regularExpression, range: nil, locale:  nil) != nil {
                let uri = self.upgradeURL(with: webAddress)
                if webViewURL.absoluteString != uri {
                    self.processRequest(from: uri)
                }
            }
            else if webAddress.range(of: httplessUriRegex, options: .regularExpression) != nil {
                let uri = self.upgradeURL(with: webAddress)
                if webViewURL.absoluteString != uri {
                    self.processRequest(from: uri)
                }
            } else {
                let search = "https://google.com/search?q=\(webAddress)"
                if webView.url?.absoluteString != search {
                    self.processRequest(from: search)
                }
            }
        }
    }
}

extension ViewController: UIScrollViewDelegate {
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if initialContentOffset > scrollView.contentOffset.y {
            scrollDirection = .up
        } else {
            scrollDirection = .down
        }
        initialContentOffset = scrollView.contentOffset.y
    }
}

extension ViewController: UIContextMenuInteractionDelegate {
    @available(iOS 13.0, *)
    func openInANewTab(action: UIAction) {
        if let link = linkMenuName {
            let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SBWebViewController") as! ViewController
            newVC.initialURL = link
            
            self.show(newVC, sender: nil)
        }
        print("Open in a new tab Action")
    }
    
    @available(iOS 13.0, *)
    func getContextMenu() -> UIMenu {
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up"), identifier: .init("share"), discoverabilityTitle: "share", state: .on) { action in
            print(action)
        }
        let openNewTab = UIAction(title: "Open in a new tab", image: UIImage(systemName: "square.and.arrow.up"), identifier: .init("openInNewTab"), discoverabilityTitle: "Open in a new tab", state: .off, handler: self.openInANewTab)
        return UIMenu(title: linkMenuName ?? "Menu", children: [action, openNewTab])
    }
    
    @available(iOS 13.0, *)
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { suggestedActions in
            return self.getContextMenu()
        } )
    }
}

class GWKNavTitleView: UIView {
    
    public lazy var title: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.textAlignment = .center
        return label
    }()
    
    public var titleString = "" {
        didSet {
            if oldValue != titleString {
                self.title.text = titleString
                self.showTitle = true
            }
        }
    }
    
    public lazy var addressBar: GWKAddressBar = {
        let textField = GWKAddressBar(frame: CGRect(x: 0, y: 0, width: 300, height: 35))
        textField.isEnabled = true
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.backgroundColor = UIColor(red: 0.50, green: 0.50, blue: 0.55, alpha: 0.09)
        textField.layer.cornerRadius = 10
        textField.clipsToBounds = true
        return textField
    }()
    
    public var showTitle = true {
        didSet {
            if oldValue != showTitle {
                self.toggleTitleVisibility()
            }
        }
    }
    
    var addressBarDelegate: UITextFieldDelegate? {
        didSet {
            self.addressBar.delegate = addressBarDelegate
        }
    }
    
    var GWKNavTitleViewConstraints = [NSLayoutConstraint]()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        prepareView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    func prepareView() {
        self.setConstraints()
        self.setGestureHandler()
    }
    
    @objc public func toggleTitleVisibility() {
        title.isHidden = !title.isHidden
        addressBar.isHidden = !addressBar.isHidden
    }
    
    func initConstraints() {
        let distanceFromMargins = CGFloat(16)

        self.GWKNavTitleViewConstraints.append(self.heightAnchor.constraint(equalToConstant: 30))
        
//        self.GWKNavTitleViewConstraints.append(self.widthAnchor.constraint(equalToConstant: 150))
//        if let superview = self.superview {
//            self.GWKNavTitleViewConstraints.append(self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 50))
//
//            self.GWKNavTitleViewConstraints.append(self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: 50))
//        }
        
        // AddressBar
        self.GWKNavTitleViewConstraints.append(self.addressBar.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        self.GWKNavTitleViewConstraints.append(self.addressBar.centerYAnchor.constraint(equalTo: self.centerYAnchor))
//        self.GWKNavTitleViewConstraints.append(self.addressBar.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: distanceFromMargins))
//        self.GWKNavTitleViewConstraints.append(self.addressBar.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: distanceFromMargins))
        self.GWKNavTitleViewConstraints.append(self.addressBar.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5))
        
        // Title
        self.GWKNavTitleViewConstraints.append(self.title.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        self.GWKNavTitleViewConstraints.append(self.title.centerYAnchor.constraint(equalTo: self.centerYAnchor))
        self.GWKNavTitleViewConstraints.append(self.title.leadingAnchor.constraint(equalTo: self.leadingAnchor))
        self.GWKNavTitleViewConstraints.append(self.title.trailingAnchor.constraint(equalTo: self.trailingAnchor))
    }
    
    func setConstraints() {
        self.addSubview(title)
        self.addSubview(addressBar)
//        self.addressBar.translatesAutoresizingMaskIntoConstraints = false
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.initConstraints()
        NSLayoutConstraint.activate(self.GWKNavTitleViewConstraints)
    }
    
    func setGestureHandler() {
        self.title.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleTitleVisibility))
        tapGesture.numberOfTapsRequired = 1
        self.title.addGestureRecognizer(tapGesture)
    }
}

class GWKAddressBar: UITextField {
    
    public var addressBarPadding = UIEdgeInsets(
        top: 5,
        left: 20,
        bottom: 5,
        right: 20
    )
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: addressBarPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: addressBarPadding)
    }
}
