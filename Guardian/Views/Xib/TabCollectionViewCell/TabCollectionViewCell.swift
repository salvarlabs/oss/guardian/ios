//
//  TabCollectionViewCell.swift
//  Guardian
//
//  Created by Saulo on 02/05/21.
//

import UIKit

class TabCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tabTitleLabel: UILabel!
    @IBOutlet weak var tabThumbnailView: UIImageView!
    var cellData: TabCollectionViewCellData? {
        didSet {
            if oldValue != cellData {
                self.updateView()
            }
        }
    }
    
    var tabData: GCDTab?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tabThumbnailView.backgroundColor = .white
        guard cellData != nil else {return}
        updateView()
        // Initialization code
    }
    
    func updateView() {
        tabThumbnailView.image = cellData?.thumbnail
        tabTitleLabel.text = cellData?.title ?? "New tab"
    }

}
