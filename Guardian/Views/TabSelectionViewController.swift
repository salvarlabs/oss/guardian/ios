//
//  TabSelectionViewController.swift
//  Guardian
//
//  Created by Saulo on 23/04/21.
//

import UIKit
import CoreData

class TabSelectionViewController: UIViewController {
    
    var tabs: [GCDTab]?
    var context: NSManagedObjectContext!
    var collectionViewCellID = "tab_collection_cell"
    lazy var grid: UICollectionView? = {
        let collection = UICollectionView(
            frame: view.frame,
            collectionViewLayout: UICollectionViewLayout()
        )
        collection.dataSource = self
        collection.setCollectionViewLayout(layout, animated: true)
        return collection
    }()
    lazy var layout: ColumnFlowLayout = {
        let layout = ColumnFlowLayout()
        layout.scrollDirection = .vertical
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let appDelegate = (UIApplication.shared.delegate as? AppDelegate) else  {
                fatalError("Data Container not found.")
        }
        context = GuardianCoreData.shared.workingContext
//        grid?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: collectionViewCellID)
        grid?.register(UINib(nibName: "TabCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: collectionViewCellID)
        do {
            tabs =  try? appDelegate.tabProvider.persistentContainer.viewContext.fetch(GCDTab.fetchRequest())
        } catch {
            print(error)
        }
        grid?.reloadData()
        print("Loaded hub")
        self.view.backgroundColor = .white
        self.view.addSubview(grid!)
        
    }
    
    func decodeImage(base64: String?) -> UIImage? {
        guard let base64 = base64 else {return nil}
        let decodedData = NSData(base64Encoded: base64, options: .ignoreUnknownCharacters)
        return UIImage(data: decodedData! as Data)
    }
    
    @objc func openTabFromTabCenter(sender: Any?) {
        print("CELL CLICKED")
        if sender is UITapGestureRecognizer {
            let tapRecognizer = sender as! UITapGestureRecognizer
            let cell = tapRecognizer.view as! TabCollectionViewCell
            print("\(String(describing: tapRecognizer.view)) detected")
            print("\(cell.tabData)")
            guard let tab = cell.tabData else {return}
            let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SBWebViewController")as! ViewController
            newVC.initialURL = tab.url
            newVC.currentTabData = tab
            self.show(newVC, sender: TabSelectionViewController.self)
        }

    }

}

extension TabSelectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TabCollectionViewCell = grid!.dequeueReusableCell(withReuseIdentifier: collectionViewCellID, for: indexPath) as! TabCollectionViewCell
        cell.layer.cornerRadius = cell.frame.width / 8
        cell.clipsToBounds = true
        
        var cellData = TabCollectionViewCellData(title: tabs?[indexPath.row].title ?? "", thumbnail: nil)
        
        if let image = decodeImage(base64: tabs?[indexPath.row].encodedImage) {
            cellData.thumbnail = image
//            let imageView = UIImageView(image: image)
//            cell.contentView.addSubview(imageView)
//            imageView.contentMode = .scaleAspectFill
//            imageView.translatesAutoresizingMaskIntoConstraints = false
//            imageView.topAnchor.constraint(equalTo: cell.topAnchor, constant: CGFloat(100)).isActive = true
//            imageView.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
//            imageView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
//            imageView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
//            imageView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
        }
        
//        let title = UILabel()
//        title.text = tabs?[indexPath.row].title
//        title.textColor = .black
//        cell.contentView.addSubview(title)
//        title.translatesAutoresizingMaskIntoConstraints = false
//        title.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
//        title.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
//        cell.layer.backgroundColor = UIColor.red.cgColor
        cell.cellData = cellData
        cell.tabData = tabs?[indexPath.row]
        let gestureRecogn = UITapGestureRecognizer(target: self, action: #selector(openTabFromTabCenter(sender:)))
        cell.addGestureRecognizer(gestureRecogn)
        cell.isUserInteractionEnabled = true
        return cell
    }
    
    
}
